import { Button } from '@mui/material';
import React from 'react';
import { useHistory } from 'react-router-dom';

const Registerpage = () => {

    const history = useHistory();

    const register = () => {
        history.push('/login');
    }


    return (
        <div>
            <h1>Register Page</h1>
            <Button variant="contained" onClick={register}>Go to Login</Button>
        </div>
    );
}

export default Registerpage;