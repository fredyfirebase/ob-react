import React from 'react';
import Loginformik from '../../components/pure/forms/loginFormik';
import { login } from '../../services/fetchService';
import Button from '@mui/material/Button';
import { useHistory } from 'react-router-dom';

const Loginpage = () => {

    const history = useHistory();

    const login = () => {
        history.push('/register');
    }

    return (
        <div>
            <h1>Login Page</h1>
            <Loginformik></Loginformik>
            <Button variant="contained" onClick={login}>Go to Register</Button>
        </div>
    );
}

export default Loginpage;

