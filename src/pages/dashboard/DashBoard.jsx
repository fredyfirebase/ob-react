import React from 'react';
import { useHistory } from 'react-router-dom';

import Button from '@mui/material/Button';

import Copyright from '../../components/pure/Copyright';
import {  ButtonGroup } from '@mui/material';

const Dashboardpage = () => {

    const history = useHistory();

    const logout = () => {
        history.push('/login');
    }

    const goTask = () => {
        history.push('/tasks')
    }

    return (
        <>
            <ButtonGroup variant="contained" aria-label="outlined primary button group">
                <Button variant="contained" onClick={logout}>Logout</Button>
                <Button variant="contained" onClick={goTask}>Go to Tasks</Button>
            </ButtonGroup>
            <Copyright></Copyright>
        </>
    );
}

export default Dashboardpage;
