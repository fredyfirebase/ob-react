import React, {useRef} from 'react';
import PropTypes from 'prop-types';
import { LEVELS } from '../../../models/levels.enum';
import { Task } from '../../../models/task.class';
import { Formik, Form, Field, ErrorMessage } from 'formik';
import * as Yup from 'yup';

const Taskform = ({add, length}) => {

    const initialValues = {
        name: '',
        description: '',
        level: LEVELS.NORMAL,
    }

    const registerSchema = Yup.object().shape(
        {
            name: Yup.string()
                .min(6, 'Name Task is short')
                .max(12, 'Name TAsk is long')
                .required('Task is required'),
            description: Yup.string()
                .min(6, 'Description is short')
                .max(12, 'Description is long')
                .required('Description is required'),
            level: Yup.string()
                .required('Level is required'),
        }
    )

    return (
        <div>
            <h4>Register Formik</h4>
            <Formik
                initialValues = {initialValues}
                // *** Yup Validation Schema ***
                validationSchema = {registerSchema}
                // ** onSubmit Event
                onSubmit={async (values) => {
                    await new Promise((r) => setTimeout(r, 1000));
                    const newTask = new Task(
                        values.name,
                        values.description,
                        false,
                        values.level
                    );
                    add(newTask);
                }}
            >

            {({ values,
                    touched,
                    errors,
                    isSubmitting,
                    handleChange,
                    handleBlur }) => (
                        <Form>
                            <Field id="name" type="text" name="name" placeholder="Add Task" />
                            {
                                errors.name && touched.name && 
                                (
                                    <ErrorMessage name="name" component='div'></ErrorMessage>
                                )
                            }

                            <Field id="description" type="text" name="description" placeholder="description" />
                            {
                                errors.description && touched.description && 
                                (
                                    <ErrorMessage name="description" component='div'></ErrorMessage>
                                )
                            }

                            <Field name="level" component="select">
                                <option value="normal">{LEVELS.NORMAL}</option>
                                <option value="urgent">{LEVELS.URGENT}</option>
                                <option value="blocking">{LEVELS.BLOCKING}</option>
                            </Field>
                            {
                                errors.level && touched.level && 
                                (
                                    <ErrorMessage name="level" component='div'></ErrorMessage>
                                )
                            } 

                            <button type="submit">{length > 0 ? 'Add New Task' : 'Create your First Task'}</button>
                            {isSubmitting ? (<p>Sending your Task...</p>): null}

                        </Form>
                    )
            }

            </Formik>
        </div>
    );
}

Taskform.protoTypes = {
    add: PropTypes.func.isRequired,
    length: PropTypes.number.isRequired
}

export default Taskform;